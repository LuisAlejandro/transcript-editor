#!/usr/bin/python3.5
# -*- coding: utf-8 -*-

import os
import sys
import pysrt
import tempfile
from webvtt import WebVTT
from argparse import ArgumentParser

if not sys.version_info < (3,):
    unicode = str
    basestring = str

# parse command line arguments
ap = ArgumentParser(description='SRT to VTT converter')
ap.add_argument('-p', help='project name')
args = ap.parse_args()

basedir = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
srtdir = os.path.abspath(
    os.path.join(basedir, 'project', args.p, 'transcripts', 'srt'))
vttdir = os.path.abspath(
    os.path.join(basedir, 'project', args.p, 'transcripts', 'webvtt'))
srts = [os.path.abspath(os.path.join(srtdir, p)) for p in os.listdir(srtdir)]

for srtfile in srts:
    print('Converting "{0}" ...'.format(srtfile))

    tmpsrtfile = tempfile.mkstemp()[1]
    vttfilename = '{0}.vtt'.format(
        os.path.splitext(os.path.basename(srtfile))[0])
    vttfile = os.path.join(vttdir, vttfilename)

    srt = pysrt.open(srtfile)
    srt.save(tmpsrtfile)

    vtt = WebVTT().from_srt(tmpsrtfile)
    vtt.save(vttfile)

    os.remove(tmpsrtfile)
